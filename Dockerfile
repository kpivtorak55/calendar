FROM python:3
ADD requirements.txt .
RUN pip install -r requirements.txt
WORKDIR .
COPY . .
ENTRYPOINT ["python3"]
CMD  ["calendar_tests.py"]


