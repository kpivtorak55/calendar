
import unittest
from calendar import Calendar

class Test_Calendar(unittest.TestCase):


    def setUp(self):
        self.my_calendar = Calendar()
   

    def test_check_even(self):

        self.my_calendar.check_even("2015, 6, 16")
        self.my_calendar.check_even("2017, 3, 16")
        with self.assertRaises(Exception): self.my_calendar.check_even(5)

   
  
  
# Executing the tests in the above test case class
if __name__ == "__main__":
  unittest.main()
